@echo off
copy /y build\src\d2gs_1.13c\Release\D2GS.exe install\1.13c\
copy /y build\src\d2warden_1.13c\Release\D2Warden.dll install\1.13c\
copy /y patch\1.13c\d2server.dll install\1.13c\
copy /y patch\1.13c\d2server.dll bin\1.13c\
copy /y build\src\d2gs_1.13d\Release\D2GS.exe install\1.13d\
copy /y build\src\d2warden_1.13d\Release\D2Warden.dll install\1.13d\
copy /y patch\1.13d\d2server.dll install\1.13d\
copy /y patch\1.13d\d2server.dll bin\1.13d\
@echo 完成，请将install对应版本目录下的所有文件复制到游戏服务器目录。
pause