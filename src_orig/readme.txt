D2GS 1.13c, 1.13a, 1.12a, 1.11b, 1.10, 1.09d, 1.09c
D2GS - Diablo II Game Server, D2GS 1.11b+ being developed by marsgod
« Back to Main Page
Navigation
Show only version: All | 1.13c | 1.13a | 1.12a | 1.11b | 1.10 | 1.09d | 1.09c
2010-03-31 - D2GS 1.13c - Build 3 (with source code) (152 541)
1. DisableDCSpawnInSomeArea=1; This will not spawn DiabloClone in 'The Chaos Sanctuary' 'Arreat Summit' and Uber Boss Levels.Because spawn DiabloClone in these levels have some bug, this is just a workaround.
2. d2warden.ini: SupportD2ClientVersion=1,2,3 #you can use this option force the player use a special d2 client version,1=1.11b 2=1.12a 3=1.13....
NOTE: To enable all 1.11b+ version join the game server, you MUST set SupportD2ClientVersion=1,2,3
3. Cast 'Cloak of Shadows' on ancient barbarians will not crash the game.

D2Loader 1.13c attached.
Download
2010-03-31 - D2GS 1.11b - Build 46 (with source code) (63 472)
1. d2warden.ini: SupportD2ClientVersion=1,2,3 #you can use this option force the player use a special d2 client version,1=1.11b 2=1.12a 3=1.13a....
2. Remove a monster damage bug introduce after build 42.....
Download
2010-03-07 - D2GS 1.11b - Build 45 (with source code) (9 777)
1. New feature:
DisableDCSpawnInSomeArea=1; This will not spawn DiabloClone in 'The Chaos Sanctuary' 'Arreat Summit' and Uber Boss Levels.Because spawn DiabloClone in these levels have some bug, this is just a workaround.
2. Cast 'Cloak of Shadows' on ancient barbarians will not crash the game.
Download
2010-01-15 - D2GS 1.13a - Build 2 (14 099)
1. New features:
EnableEthSocketBugFix=1 ; This will disable additional 50% reward of eth armor when use the cube.
DisableBugMF=1 ; This will disable all ACT boss quest drop within a non-quest drop game.
2. d2warden.ini update: you can detect Redvex plugin now. See [Redvex1][Redvex2] in d2warden.ini
Download
2010-01-15 - D2GS 1.11b - Build 44 (with source code) (7 616)
1. New features:
EnableEthSocketBugFix=1 ; This will disable additional 50% reward of eth armor when use the cube.
DisableBugMF=1 ; This will disable all ACT boss quest drop within a non-quest drop game.
2. d2warden.ini update: you can detect Redvex plugin now. See [Redvex1][Redvex2] in d2warden.ini
Download
2009-12-13 - D2GS 1.13a - Build 1 (6 315)
All patches in D2GS 1.11b build 43 applied to this server, except the USC trade window patch.
Download
2009-03-12 - D2GS 1.11b - Source code (10 229)
Source Release - 2009-03-12
Download
2009-03-01 - D2GS 1.11b - Build 43 (15 752)
1. Make the d2warden more stable;
2. d2warden can support MM.bot check now. Please set EnableMMBotCheck in your d2warden.ini;
3. New Feature! EnablePreCalculateTCNoDropTbl, When set to 1, the multiply players TC NoDrop floating-point calculation will be pre-calculated, and this can improve your server performance;
4. Remove unnecessary Monster Damage calculation from server side;
5. Slightly modify the Uber Tristram boss's behavior.
Download
2009-02-17 - D2GS 1.11b - Build 42 (6 728)
Only start warden after player enter game. Hope this can fix some crash/corrupt problems.
Download
2009-02-13 - D2GS 1.11b - Build 41 (7 136)
Fix a bug in d2warden, which will cause the NEC set create a no Animation hack log.
Download
2009-02-11 - D2GS 1.11b - Build 40 (5 576)
Greatly enchance d2warden. Now it can detect following hack or bot:
1. packet based bot, such as kuk-bot(EnablePlayerMouseCheck=1);
2. js32 based bot, such as d2jsp based bot, NT bot;
3. d2launch with cguard support. cguard cheat the warden, so don't use cguard;
4. TMCBP Mod, which modify the client side AnimData.d2;
5. d2hackit based hack or bot;
6. some drop notify hack, pickit?
7. other anti-warden hack or bot;
8. d2hackmap, if you want... :)
Download
2009-02-01 - D2GS 1.11b - Build 39 (3 720)
NewFeatures! You can enable warden in d2gs now! Please set EnableWarden=1 in NewFeatures section.
Currently, the warden support detect the d2jsp and d2hackmap in client side.
You can configure the warden in d2warden.ini file.
Warden log will be in d2warden.log, including the player kick log.
NOTE: This use the BN mod for warden work, no need to update your d2loader or client side program.
Download
2008-11-24 - D2GS 1.11b - Build 38 (9 058)
Fix a crash bug when reloading the config file.
Download
2008-11-23 - D2GS 1.11b - Build 37 (3 728)
You can enable DiabloClone spawned only in the game where the key sold.
Add "DcItemRate=1000" in your d2server.ini [World Event] section. Where 1000 mean 1000/1000=100% problity spawn DiabloClone in game. When you set DcItemRate=0, this will disable this feature.
Download
2008-11-23 - D2GS 1.09d - Build 8 (7 037)
Three bugs fixed.
Download
2008-08-26 - D2GS 1.11b - Build 36 (5 641)
When EnableUnicodeCharName=1, you pickup a Ear or Personalize your item will not cause a save data corrupt. (But your unicode name on ear or item will not display correctly, this is because the save file use 7 bits save char name, not 8 bits.)
Download
2008-07-14 - D2GS 1.11b - Build 35 (5 480)
Now the server support Unicode Char Name.
Please set EnableUnicodeCharName=1 in your d2server.ini.
For Unicode Char Name support, you need a modified version of D2CS.
For more detail, please check the UnicodeCharName directory.
Download
2008-07-11 - D2GS 1.11b - Build 34 (4 316)
You can now set some new features via the d2server.ini, please check the d2server.ini file!

    EnableMeleeHireableAI: When set to 1, the ACT2&ACT5 hireable's AI will be fixed, when the hireable is cursed by IM, he will stop attack monster, until the cruse disappear or replace by another curse.(Your melee pet will be more safer!)
    EnableNeroPetAI: When set to 1, the Nero Golem's AI will be fixed, when the golem is cursed by IM, it will stop attack monster, until the cruse disappear or replace by another curse.(Your Pride Nero Iron Golem will be more safer!)
    EnableExpGlitchFix: When set to 1, the experience that player get when in party will be fixed.(The orig program has some bug, you will get only 1 exp when in 8pp party killing hell diablo.)
    DisableUberUp: When set to 1, you will get 0 exp when killing the monster summon by Uber Boss(including Uber Meph and Uber Diablo). This will prevent the Uber Up.

Also, these new features will log in to d2ge.log file.
Download
2008-07-09 - D2GS 1.11b - Build 33 (3 990)
Fix the Experience Glitch.
For more info,please check: link, link.
Also, this should work for classic game when 8pp party kill Diablo. No need leave party anymore. All players will get correct experience.
Download
2008-06-28 - D2GS 1.11b - Build 32 (5 086)
Now the classic dru or classic asn can't enter game anymore.
Also, you can't create a classic dru or classic asn.
Download
2008-06-19 - D2GS 1.12a - Build 1 (19 519)
First Release...
Download
2008-05-10 - D2GS 1.11b - Build 31 (5 147)
Fixed the Aura bug caused by PET &amp; Player Trade.
The old method has some bug... ^_^
Download
2008-05-03 - D2GS 1.11b - Build 30 (4 565)
Creating a town portal when corpses or other objects entirely fill the area where the portal will appear in town will no longer crash the game.
Download
2007-10-23 - D2GS 1.11b - Build 29 (12 792)
Fix a bug when trying to create a second Final Portal.
The trade player aura bug has been fixed.
Download
2007-08-31 - D2GS 1.11b - Build 28 (8 606)
This build removes the re-based storm.dll.
It can use the offical storm.dll now (RECOMMEND).
Download
2007-07-05 - D2GS 1.11b - Build 27 (5 344)
Uber Quest rewriting.
Download
2007-07-04 - D2GS 1.11b - Build 26 (4 020)
Rewriting all function call stack.
Download
2007-07-03 - D2GS 1.11b - Build 25 (4 062)
Mini bug fixed
Download
2007-07-03 - D2GS 1.11b - Build 24 (3 980)
FOG.dll an increase in memory management.
Download
2007-07-02 - D2GS 1.11b - Build 23 (4 006)
Wrong version can make only one game, don't use it.
Download
2007-07-02 - D2GS 1.11b - Build 22 (3 902)
Help the stability of the server.
Download
2007-06-16 - D2GS 1.11b - Build 21 (4 282)
Download
2007-06-04 - D2GS 1.11b - Build 20 (4 944)
Download
2004-03-04 - D2GS 1.10 - Build 6 (11 395)
Download
2004-01-06 - D2GS 1.10 - Build 4 (6 371)
Download
2002-04-11 - D2GS 1.09d - Build 7 (4 461)
Download
2002-01-07 - D2GS 1.09d - Build 6 (3 429)
Download
2001-12-11 - D2GS 1.09d - Build 5 (3 458)
Download
2001-12-06 - D2GS 1.09d - Build 4 (3 475)
Download
2001-11-22 - D2GS 1.09c - Build 4 (4 511)
Download
2001-11-22 - D2GS 1.09c - Build 3 (3 465)
Download
2001-10-22 - D2GS 1.09d - Build 2 (3 793)
Download
Informations
Valid xHTML 1.1, CSS 2.1.
Info 556326 hits, 194822 visits, 476824 total downloads, generated in 9.8 ms.
