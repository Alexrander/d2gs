add_executable(D2GS_13c WIN32
	../d2gs_src/bn_types.h
	../d2gs_src/bnethash.c
	../d2gs_src/bnethash.h
	../d2gs_src/callback.c
	../d2gs_src/callback.h
	../d2gs_src/charlist.c
	../d2gs_src/charlist.h
	../d2gs_src/config.c
	../d2gs_src/config.h
	../d2gs_src/connection.h
	../d2gs_src/d2cs_d2gs_character.h
	../d2gs_src/d2cs_d2gs_protocol.h
	../d2gs_src/d2dbs_d2gs_protocol.h
	../d2gs_src/d2gamelist.c
	../d2gs_src/d2gamelist.h
	../d2gs_src/d2ge.c
	../d2gs_src/d2ge.h
	../d2gs_src/d2gs.h
	../d2gs_src/debug.c
	../d2gs_src/debug.h
	../d2gs_src/eventlog.c
	../d2gs_src/eventlog.h
	../d2gs_src/handle_s2s.c
	../d2gs_src/handle_s2s.h
	../d2gs_src/hexdump.c
	../d2gs_src/hexdump.h
	../d2gs_src/list.h
	../d2gs_src/main.c
	../d2gs_src/net.c
	../d2gs_src/net.h
	../d2gs_src/psapi.h
	../d2gs_src/telnetd.c
	../d2gs_src/telnetd.h
	../d2gs_src/timer.c
	../d2gs_src/timer.h
	../d2gs_src/utils.c
	../d2gs_src/utils.h
	../d2gs_src/vars.c
	../d2gs_src/vars.h
	../d2gs_src/versioncheck.c
	../d2gs_src/versioncheck.h
	../d2gs_src/D2Server.ico
	../d2gs_src/d2server.rc
	../d2gs_src/resource.h)

add_custom_command(TARGET D2GS_13c 
    PRE_BUILD 
    COMMAND lib /def:${PROJECT_SOURCE_DIR}/libs/1.13c/d2server.def /machine:i386 /out:${PROJECT_SOURCE_DIR}/libs/1.13c/d2server.lib
    VERBATIM
    )

target_include_directories(D2GS_13c PRIVATE ${PROJECT_SOURCE_DIR}/include/ ${PROJECT_SOURCE_DIR}/resources/)

target_compile_definitions(D2GS_13c PRIVATE D2GS)

target_link_libraries(D2GS_13c PRIVATE ws2_32 ${PROJECT_SOURCE_DIR}/libs/1.13c/d2server.lib)

set_target_properties(D2GS_13c
	PROPERTIES
		C_STANDARD 99
		C_STANDARD_REQUIRED YES
		C_EXTENSIONS NO
		OUTPUT_NAME "D2GS")
